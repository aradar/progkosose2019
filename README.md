# Dependencies
The project needs the following packages installed:
- OpenCL Headers
- A OpenCL ICD loader
- A working OpenCL Platform
- OpenCV (optional OpenCV parts which where needed during linking against OpenCV: hdf5 and gtk3)
- CMake
- As all binary files in the repo are checked in through git lfs you can also count it as a dependency
