#include <iostream>
#include <vector>
#include <chrono>

#include "cli/cli_options.hpp"
#include "backends/opencv_computation_backend.hpp"
#include "backends/opencl_computation_backend.hpp"


int main(int argc, char **argv) {
    auto start_app_time = std::chrono::high_resolution_clock::now();

    auto options = CliOptions(argc, argv);

    if (options.get_help_requested()) {
        return 0;
    }

    ComputationBackend *backend;
    switch (options.get_backend()) {
        case AvailableComputationBackend::OPENCV:
            backend = new OpenCvComputationBackend();
            break;

        case AvailableComputationBackend::OPENCL:
            backend = new OpenClComputationBackend(options.get_platform_id(), options.get_device_id());
            break;
    }

    if (options.get_print_opencl_platforms() || options.get_print_opencl_devices()) {
        if (options.get_backend() != AvailableComputationBackend::OPENCL) {
            std::cout << "OpenCL platforms and devices can only be printed if also the OpenCL backend is used!\n";
            return -1;
        }

        if (options.get_print_opencl_platforms()) {
            OpenClComputationBackend::print_available_platforms();
        } else {
            OpenClComputationBackend::print_available_devices(options.get_platform_id());
        }

        delete (backend);  // mem cleanup
        return 0;
    }

    std::chrono::nanoseconds calc_time_delta_sum(0);
    for (int i = 0; i < options.get_no_benchmark_repeat_calc(); i++) {
        auto input = cv::imread(options.get_input_path(), -1); // this preserves the alpha channel if one exists
        cv::cvtColor(input, input, cv::COLOR_BGRA2RGBA);
        auto output = cv::Mat(input.size(), input.type());

        auto calc_time_start = std::chrono::high_resolution_clock::now();

        switch (options.get_task()) {
            case AvailableTasks::RGB_TO_HSV:
                backend->rgba_to_hsva(input, output);
                break;

            case AvailableTasks::RGB_TO_GRAYSCALE:
                backend->rgba_to_grayscale(input, output);
                break;

            case AvailableTasks::EMBOSS_FILTER:
                backend->apply_emboss_filter(input, output);
                break;

            default:
                return -1;
        }

        auto calc_time_end = std::chrono::high_resolution_clock::now();
        calc_time_delta_sum += calc_time_end - calc_time_start;

        if (options.get_output_path() != "") {
            cv::imwrite(options.get_output_path(), output);
        }
    }

    backend->free_resources();
    delete (backend);

    auto end_app_time = std::chrono::high_resolution_clock::now();
    auto complete_app_time = end_app_time - start_app_time;

    std::cout << std::scientific << std::setprecision(4);

    std::cout << "Complete execution time: " << static_cast<double>(complete_app_time.count()) << " ns\n";
    std::cout << "Calculation only time: " << static_cast<double>(calc_time_delta_sum.count()) << " ns\n";

    return 0;
}