#include <iostream>
#include "cli_options.hpp"

std::istream &operator>>(std::istream &in, AvailableComputationBackend &backend) {
    std::string token;
    in >> token;
    std::transform(token.begin(), token.end(), token.begin(), ::tolower);

    if (token == "opencl" || token == "0")
        backend = AvailableComputationBackend::OPENCL;
    else if (token == "opencv" || token == "1")
        backend = AvailableComputationBackend::OPENCV;
    else
        in.setstate(std::ios_base::failbit);

    return in;
}

std::istream &operator>>(std::istream &in, AvailableTasks &task) {
    std::string token;
    in >> token;
    std::transform(token.begin(), token.end(), token.begin(), ::tolower);

    if (token == "rgb-to-hsv" || token == "0")
        task = AvailableTasks::RGB_TO_HSV;
    else if (token == "rgb-to-grayscale" || token == "1")
        task = AvailableTasks::RGB_TO_GRAYSCALE;
    else if (token == "emboss-filter" || token == "2")
        task = AvailableTasks::EMBOSS_FILTER;
    else
        in.setstate(std::ios_base::failbit);

    return in;
}

void CliOptions::Parse(int argc, char **argv) {
    po::options_description desc("Allowed options");
    desc.add_options()
            ("help,h", "img-manipulator help screen")
            ("task,t", po::value<AvailableTasks>(&task)->default_value(AvailableTasks::RGB_TO_HSV),
             "sets the used manipulation task. Currently available tasks are: rgb-to-grayscale, rgb-to-hsv and emboss-filter")
            ("backend,b",
             po::value<AvailableComputationBackend>(&backend)->default_value(AvailableComputationBackend::OPENCL),
             "sets the used backend for computations. Currently available backends are: opencl and opencv")
            ("output-path,o", po::value<std::string>(&output_path)->default_value(""),
             "path to the output file.")
            ("input-path,i", po::value<std::string>(&input_path)->default_value(""),
             "path to the input file")
            ("platform-id,p", po::value<int>(&platform_id)->default_value(0),
             "Sets the platform ID which gets used for the OpenCL computing")
            ("device-id,d", po::value<int>(&device_id)->default_value(0),
             "Sets the device ID which gets used for the OpenCL computing")
            ("print-opencl-platforms", po::bool_switch(&print_opencl_platforms)->default_value(false),
             "Prints all available OpenCL platforms and does no computing")
            ("print-opencl-devices", po::bool_switch(&print_opencl_devices)->default_value(false),
             "Prints all available OpenCL devices for the specified platform and does no computing")
            ("no-benchmark-repeat-calc,r", po::value<int>(&no_benchmark_repeat_calc)->default_value(1),
             "Sets the number of loop runs over the given image for benchmarking purposes");

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    if (vm.count("help")) {
        help_requested = true;
    }
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << desc << "\n";
        return;
    }
}

AvailableTasks CliOptions::get_task() {
    return task;
}

AvailableComputationBackend CliOptions::get_backend() {
    return backend;
}

std::string CliOptions::get_input_path() {
    return input_path;
}

std::string CliOptions::get_output_path() {
    return output_path;
}

bool CliOptions::get_print_opencl_platforms() {
    return print_opencl_platforms;
}

bool CliOptions::get_print_opencl_devices() {
    return print_opencl_devices;
}

int CliOptions::get_platform_id() {
    return platform_id;
}

int CliOptions::get_device_id() {
    return device_id;
}

int CliOptions::get_no_benchmark_repeat_calc() {
    return no_benchmark_repeat_calc;
}

bool CliOptions::get_help_requested() {
    return help_requested;
}
