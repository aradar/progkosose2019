#include <utility>

#ifndef M11_PROGKO_SOSE19_CLI_OPTIONS_H
#define M11_PROGKO_SOSE19_CLI_OPTIONS_H

#include <string>
#include <boost/program_options.hpp>

namespace po = boost::program_options;

enum AvailableComputationBackend {
    OPENCL, OPENCV
};

enum AvailableTasks {
    RGB_TO_HSV, RGB_TO_GRAYSCALE, EMBOSS_FILTER
};

class CliOptions {
    std::string input_path;
    std::string output_path;
    AvailableComputationBackend backend;
    AvailableTasks task;
    int platform_id;
    int device_id;
    bool print_opencl_platforms;
    bool print_opencl_devices;
    int no_benchmark_repeat_calc;
    bool help_requested;

    void Parse(int argc, char *argv[]);

public:

    CliOptions(int argc, char *argv[])
            : input_path(""),
              output_path(""),
              backend(AvailableComputationBackend::OPENCL),
              task(AvailableTasks::RGB_TO_HSV),
              platform_id(0),
              device_id(0),
              print_opencl_platforms(false),
              print_opencl_devices(false),
              no_benchmark_repeat_calc(1),
              help_requested(false) {
        Parse(argc, argv);
    }

    ~CliOptions() = default;

    AvailableTasks get_task();

    AvailableComputationBackend get_backend();

    std::string get_input_path();

    std::string get_output_path();

    int get_platform_id();

    int get_device_id();

    bool get_print_opencl_platforms();

    bool get_print_opencl_devices();

    int get_no_benchmark_repeat_calc();

    bool get_help_requested();

};

#endif //M11_PROGKO_SOSE19_CLI_OPTIONS_H
