#ifndef M11_PROGKO_SOSE19_COMPUTATION_BACKEND_HPP
#define M11_PROGKO_SOSE19_COMPUTATION_BACKEND_HPP

#include <opencv2/opencv.hpp>

class ComputationBackend {

public:
    ComputationBackend() = default;

    virtual ~ComputationBackend() = default;

    virtual void rgba_to_hsva(cv::Mat &input, cv::Mat &output) = 0;

    virtual void rgba_to_grayscale(cv::Mat &input, cv::Mat &output) = 0;

    virtual void apply_emboss_filter(cv::Mat &input, cv::Mat &output) = 0;

    virtual void free_resources() {};
};

#endif //M11_PROGKO_SOSE19_COMPUTATION_BACKEND_HPP
