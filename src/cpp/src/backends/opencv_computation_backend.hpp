#ifndef M11_PROGKO_SOSE19_OPENCV_COMPUTATION_BACKEND_HPP
#define M11_PROGKO_SOSE19_OPENCV_COMPUTATION_BACKEND_HPP

#include <vector>
#include "computation_backend.hpp"

class OpenCvComputationBackend : public ComputationBackend {

public:
    OpenCvComputationBackend() = default;

    ~OpenCvComputationBackend() override = default;

    void rgba_to_hsva(cv::Mat &input, cv::Mat &output) override;

    void rgba_to_grayscale(cv::Mat &input, cv::Mat &output) override;

    void apply_emboss_filter(cv::Mat &input, cv::Mat &output) override;
};

#endif //M11_PROGKO_SOSE19_OPENCV_COMPUTATION_BACKEND_HPP
