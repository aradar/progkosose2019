#include "opencv_computation_backend.hpp"

void restore_alpha_channel(cv::Mat &input, cv::Mat &output, bool stretch_channels) {
    auto alpha_channel = cv::Mat();
    cv::extractChannel(input, alpha_channel, 3);

    auto channels = std::vector<cv::Mat>();
    cv::split(output, channels);

    if (stretch_channels) {
        channels.push_back(channels[0]);
        channels.push_back(channels[0]);
    }

    channels.push_back(alpha_channel);
    cv::merge(channels, output);
}

void OpenCvComputationBackend::rgba_to_hsva(cv::Mat &input, cv::Mat &output) {
    cv::cvtColor(input, output, cv::COLOR_RGB2HSV);
    restore_alpha_channel(input, output, false);
}

void OpenCvComputationBackend::rgba_to_grayscale(cv::Mat &input, cv::Mat &output) {
    cv::cvtColor(input, output, cv::COLOR_RGBA2GRAY);
    restore_alpha_channel(input, output, true);
}

int max_1d_mat_values_index(cv::Mat &input) {
    uchar max = 0;
    int index = -1;
    auto row = input.ptr<uchar>(0);
    for (int column = 0; column < input.cols; ++column) {
        auto value = row[column];
        if (value >= max) {
            max = value;
            index = column;
        }
    }

    return index;
}

void read_from_pointer(const uchar *source, int *destination, int amount) {
    for (int i = 0; i < amount; i++) {
        destination[i] = source[i];
    }
}

void subtract_arrays(const int *a, const int *b, int *result, int length) {
    for (int i = 0; i < length; i++) {
        result[i] = a[i] - b[i];
    }
}

void abs_array(int *a, int *result, int length) {
    for (int i = 0; i < length; i++) {
        result[i] = std::abs(a[i]);
    }
}

int max_value_index_array(const int *a, int length) {
    int max = -1;
    int index = -1;
    for (int i = 0; i < length; ++i) {
        if (a[i] >= max) {
            max = a[i];
            index = i;
        }
    }

    return index;
}

void OpenCvComputationBackend::apply_emboss_filter(cv::Mat &input, cv::Mat &output) {
    output = cv::Mat(input.rows, input.cols, CV_8U);

    int pixel_values[3];
    int n_pixel_values[3];
    int diff_vec[3];
    int abs_diff_vec[3];
    for (int row = 0; row < input.rows; ++row) {
        auto n_row = row > 0 ? row - 1 : 0;
        auto *values = input.ptr<uchar>(row);
        auto *n_values = input.ptr<uchar>(n_row);
        for (int column = 0; column < input.cols; ++column) {
            auto n_column = column > 0 ? column - 1 : 0;

            read_from_pointer(&values[column * 4], pixel_values, 3);
            read_from_pointer(&n_values[column * 4], n_pixel_values, 3);

            subtract_arrays(pixel_values, n_pixel_values, diff_vec, 3);
            abs_array(diff_vec, abs_diff_vec, 3);

            int new_value = diff_vec[max_value_index_array(abs_diff_vec, 3)];
            new_value = new_value + 127 >= 255 ? 255 : new_value + 127;
            output.at<uchar>(row, column) = new_value;

            //auto pixel_values = cv::Mat(1, 3, CV_8U, &values[column * 4]);
            //pixel_values.convertTo(pixel_values, CV_16S);
            //auto n_pixel_values = cv::Mat(1, 3, CV_8U, &n_values[n_column * 4]);
            //n_pixel_values.convertTo(n_pixel_values, CV_16S);

            //cv::Mat diff_vec = pixel_values - n_pixel_values;
            //cv::Mat abs_diff_vec = cv::abs(diff_vec);

            //short new_value = diff_vec.at<short>(0, max_1d_mat_values_index(abs_diff_vec));
            //new_value = new_value + 127 >= 255 ? 255 : new_value + 127;

            //output.at<uchar>(row, column) = new_value;
        }
    }

    restore_alpha_channel(input, output, true);
}

