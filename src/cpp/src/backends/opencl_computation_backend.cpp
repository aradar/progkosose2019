#include "opencl_computation_backend.hpp"
#include <exception>

std::vector<cl::Platform> get_available_platforms() {
    std::vector<cl::Platform> platforms;
    cl::Platform::get(&platforms);

    return platforms;
}

std::vector<cl::Device> get_available_devices(const cl::Platform &platform) {
    std::vector<cl::Device> devices;
    platform.getDevices(CL_DEVICE_TYPE_ALL, &devices);

    return devices;
}

void OpenClComputationBackend::setup_opencl() {
    platform = get_available_platforms()[platform_id];
    device = get_available_devices(platform)[device_id];
    context = cl::Context(device);

    auto exe_path = boost::filesystem::read_symlink("/proc/self/exe");
    auto kernel_file_path = exe_path.parent_path() / "kernels.cl";
    std::ifstream kernel_stream(kernel_file_path.string());
    std::string kernel_src((std::istreambuf_iterator<char>(kernel_stream)), std::istreambuf_iterator<char>());
    sources.push_back({kernel_src});

    program = cl::Program(context, sources);
    try {
        program.build({device});
    }
    catch (const cl::BuildError &e) {
        for (auto const &error_pair: e.getBuildLog()) {
            std::cout << error_pair.second << std::endl;
        }
        throw std::runtime_error("Compiling the kernel code caused an fatal error");
    }

    queue = cl::CommandQueue(context, device);
}

void OpenClComputationBackend::run_kernel(cv::Mat &input, cv::Mat &output, const std::string& kernel_name) {
    int width = input.size().width;
    int height = input.size().height;

    cl::Image2D input_cl_buffer(context, CL_MEM_READ_ONLY, cl::ImageFormat(CL_RGBA, CL_UNORM_INT8), width, height);
    cl::Image2D output_cl_buffer(context, CL_MEM_READ_WRITE, cl::ImageFormat(CL_RGBA, CL_UNORM_INT8), width, height);

    cl::Kernel kernel(program, kernel_name.c_str());
    kernel.setArg(0, input_cl_buffer);
    kernel.setArg(1, output_cl_buffer);

    queue.enqueueWriteImage(
            input_cl_buffer,
            CL_TRUE,
            {0, 0, 0},
            {static_cast<unsigned long>(width), static_cast<unsigned long>(height), 1},
            0,
            0,
            input.data);

    queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(width, height));

    queue.enqueueReadImage(
            output_cl_buffer,
            CL_TRUE,
            {0, 0, 0},
            {static_cast<unsigned long>(width), static_cast<unsigned long>(height), 1},
            0,
            0,
            output.data);
}

void OpenClComputationBackend::rgba_to_hsva(cv::Mat &input, cv::Mat &output) {
    run_kernel(input, output, "rgba_to_hsva");
}

void OpenClComputationBackend::rgba_to_grayscale(cv::Mat &input, cv::Mat &output) {
    run_kernel(input, output, "rgba_to_grayscale");
}

void OpenClComputationBackend::apply_emboss_filter(cv::Mat &input, cv::Mat &output) {
    run_kernel(input, output, "rgba_emboss_filter");
}

void OpenClComputationBackend::print_available_platforms() {
    auto platforms = get_available_platforms();

    for (int i = 0; i < platforms.size(); i++) {
        std::cout << "Platform " << i << ":"
                  << " name = " << platforms[i].getInfo<CL_PLATFORM_NAME>() << ","
                  << " vendor = " << platforms[i].getInfo<CL_PLATFORM_VENDOR>() << ","
                  << " version = " << platforms[i].getInfo<CL_PLATFORM_VERSION>() << ","
                  << " profile = " << platforms[i].getInfo<CL_PLATFORM_PROFILE>()
                  << std::endl;
    }
}

void OpenClComputationBackend::print_available_devices(int platform_id) {
    print_available_devices(get_available_platforms()[platform_id]);
}

void OpenClComputationBackend::print_available_devices(const cl::Platform &platform) {
    auto devices = get_available_devices(platform);
    for (int i = 0; i < devices.size(); i++) {
        std::cout << "Device " << i << ":"
                  << " name = " << devices[i].getInfo<CL_DEVICE_NAME>() << ","
                  << " type = " << devices[i].getInfo<CL_DEVICE_TYPE>() << ","
                  << " max compute units = " << devices[i].getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>() << ","
                  << " memory (GB) = " << devices[i].getInfo<CL_DEVICE_GLOBAL_MEM_SIZE>() / 1024. / 1024. / 1024. << ","
                  << " available = " << devices[i].getInfo<CL_DEVICE_AVAILABLE>()
                  << " address bits = " << devices[i].getInfo<CL_DEVICE_ADDRESS_BITS>() << ","
                  << " image support = " << devices[i].getInfo<CL_DEVICE_IMAGE_SUPPORT>() << ","
                  << " max image2d width = " << devices[i].getInfo<CL_DEVICE_IMAGE2D_MAX_WIDTH>() << ","
                  << " max image2d height = " << devices[i].getInfo<CL_DEVICE_IMAGE2D_MAX_HEIGHT>()
                  << std::endl;
    }
}

void OpenClComputationBackend::free_resources() {
    queue.finish();
}
