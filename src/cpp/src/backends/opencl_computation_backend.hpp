#ifndef M11_PROGKO_SOSE19_OPENCL_COMPUTATION_BACKEND_HPP
#define M11_PROGKO_SOSE19_OPENCL_COMPUTATION_BACKEND_HPP

// version > 120 break nvidia based systems and setting the target version is not possible without also setting the
// minimum version
#define CL_HPP_MINIMUM_OPENCL_VERSION 120
#define CL_HPP_TARGET_OPENCL_VERSION 120
#define CL_HPP_ENABLE_EXCEPTIONS

#include <CL/cl2.hpp>
#include <boost/filesystem.hpp>

#include "computation_backend.hpp"

class OpenClComputationBackend : public ComputationBackend {

private:
    cl::Context context;
    cl::Platform platform;
    cl::Device device;
    cl::Program::Sources sources;
    cl::CommandQueue queue;
    cl::Program program;
    int platform_id;
    int device_id;

    void setup_opencl();

    void run_kernel(cv::Mat &input, cv::Mat &output, const std::string& kernel_name);

public:
    OpenClComputationBackend(int platform_id, int device_id)
            : context(),
              platform(),
              device(),
              sources(),
              queue(),
              program(),
              platform_id(platform_id),
              device_id(device_id) {

        setup_opencl();
    }

    ~OpenClComputationBackend() override = default;

    void rgba_to_hsva(cv::Mat &input, cv::Mat &output) override;

    void rgba_to_grayscale(cv::Mat &input, cv::Mat &output) override;

    void apply_emboss_filter(cv::Mat &input, cv::Mat &output) override;

    void free_resources() override;

    static void print_available_platforms();

    static void print_available_devices(int platform_id);

    static void print_available_devices(const cl::Platform &platform);
};

#endif //M11_PROGKO_SOSE19_OPENCL_COMPUTATION_BACKEND_HPP
