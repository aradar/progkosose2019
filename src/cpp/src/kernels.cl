__constant sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_NEAREST;

float3 convert_rgb_to_hsv(float red, float green, float blue) {
    const float max_value = max(red, max(green, blue));
    const float min_value = min(red, min(green, blue));
    const float delta_value = max_value - min_value;

    float hue = 0.0;
    if (max_value == min_value) {
        hue = 0.0;
    }
    else if (max_value == red) {
        hue = fmod(60.0 * ((green - blue) / delta_value) + 360.0, 360.0) / 360.0;
    }
    else if (max_value == green) {
        hue = fmod(60.0 * ((blue - red) / delta_value) + 120.0, 360.0) / 360.0;
    }
    else if (max_value == blue) {
        hue = fmod(60.0 * ((red - green) / delta_value) + 240.0, 360) / 360.0;
    }

    float saturation = 0.0;
    if (max_value > 0) {
        saturation = delta_value / max_value;
    }

    float value = max_value;

    return (float3)(hue, saturation, value);
}

__kernel void rgba_to_hsva(read_only image2d_t rgba_image, write_only image2d_t hsva_image) {
    const int x = get_global_id(0);
    const int y = get_global_id(1);

    const float4 rgba_values = read_imagef(rgba_image, sampler, (int2)(x, y));
    const float3 hsv_values = convert_rgb_to_hsv(rgba_values.x, rgba_values.y, rgba_values.z);
    const float4 hsva_values = (float4)(hsv_values, rgba_values.w);

    write_imagef(hsva_image, (int2)(x, y), hsva_values);
}

__kernel void rgb_to_hsv(read_only image2d_t rgb_image, write_only image2d_t hsv_image) {
    const int x = get_global_id(0);
    const int y = get_global_id(1);

    const float4 rgb_values = read_imagef(rgb_image, sampler, (int2)(x, y));
    const float4 hsv_values = (convert_rgb_to_hsv(rgb_values.x, rgb_values.y, rgb_values.z), 1);

    write_imagef(hsv_image, (int2)(x, y), hsv_values);
}

__kernel void rgba_to_grayscale(read_only image2d_t rgba_image, write_only image2d_t grayscale_image) {
    const int x = get_global_id(0);
    const int y = get_global_id(1);

    const float4 rgba_values = read_imagef(rgba_image, sampler, (int2)(x, y));
    const float grayscale_value = rgba_values.x * 0.21 + rgba_values.y * 0.72 + rgba_values.z * 0.07;

    write_imagef(grayscale_image, (int2)(x, y), (float4)((float3)grayscale_value, rgba_values.w));
}

__kernel void rgba_emboss_filter(read_only image2d_t rgba_image, write_only image2d_t grayscale_image) {
    const int x = get_global_id(0);
    const int y = get_global_id(1);

    const float4 values = read_imagef(rgba_image, sampler, (int2)(x, y));
    const float4 neighbor_values = read_imagef(rgba_image, sampler, (int2)(x - 1, y - 1));

    const float4 diff_vec = values - neighbor_values;
    const float4 abs_diff_vec = fabs(values - neighbor_values);

    const float max_abs_diff = max(abs_diff_vec.x, max(abs_diff_vec.y, abs_diff_vec.z));
    float diff = 0.0;
    if (abs_diff_vec.x == max_abs_diff) {
        diff = diff_vec.x;
    }
    else if (abs_diff_vec.y == max_abs_diff) {
        diff = diff_vec.y;
    }
    else {
        diff = diff_vec.z;
    }

    float gray_value = 0.5 + diff;

    write_imagef(grayscale_image, (int2)(x, y), (float4)((float3)gray_value, values.w));
}
