\input{./settings.tex}

\begin{document}
    \input{./titlepage.tex}
    
    \tableofcontents
    \pagebreak

    \section{Aufgabenstellung}
    Im Rahmen des Moduls Programmierkonzepte und Algorithmen (SoSe2019) sollte in diesem Semester von uns (Nora Kießling,
    Ralph Schlett) die folgende Aufgabe im Bereich der Bildbearbeitung (Aufgabe 8) umgesetzt werden: 

    Ziel war es eine Software zur Bearbeitung von RGB-Bildern (, die mithilfe einer Bibliothek wie pnglite, libpng,
    OpenCV eingelesen werden können) zu implementieren. Diese musste in der Lage sein, Farbraumkonvertierungen von RGB zu
    HSV und von RGB zu Grayscale durchzuführen und einen Emboss-Filter anzuwenden. Alle für diese Aufgaben benötigten
    Berechnungen sollten mithilfe von OpenCL parallelisiert durchgeführt werden. Die durch die Bearbeitung generierten
    Bilder sollten dann ebenfalls als Bilddateien speicherbar sein.

    \section{Lösung}
    Zur Lösung der Aufgabe wurde das img-manipulator Programm entwickelt. Dieses akzeptiert als Eingabe ein RGB-Bild
    beliebiger Größe und enthält Implementierungen, die es erlauben, dessen Farbraum in Grayscale oder HSV zu
    transformieren oder auch den Emboss-Filter auf das Bild anzuwenden. Das Programm lässt sich komfortabel über ein CLI
    (Command-Line Interface) bedienen. Es wurde mit Hilfe der Programmiersprache C++ entwickelt, da durch diese der
    Zugriff auf die offizielle OpenCL-Schnittstelle direkt möglich ist. Im Folgenden werden das Command-Line Interface
    sowie die einzelnen Komponenten des Programms, deren mathematische Grundlagen sowie Implementierung im Detail
    beschrieben.

    \subsection{Aufbau des Programms} \label{sec:aufbau}
    Der Aufbau des Programms ist in \autoref{fig:uml} Klassendiagramm dargestellt. Das CLI ermöglicht dem Nutzer durch einige
    im folgenden Abschnitt näher beschriebene Optionen die Angabe eines zu bearbeitenden Bildes, der gewünschten Art
    der Bearbeitung und des gewünschten Backends.  

    Unter Beachtung der vom Nutzer angegebenen Optionen wird durch die main.cpp das angegebene Backend erstellt, das
    entsprechende Bild eingelesen, dessen gewünschte Bearbeitung angestoßen und zum Schluss das Ergebnis in Form eines
    bearbeiteten Bildes zurückgegeben.

    Für die eigentlichen Berechnungen der Farbraumtransformationen bzw. der Anwendung des Emboss-Filters wurde auf der
    einen Seite ein OpenCL-Backend sowie auf der anderen Seite zu Vergleichszwecken ein OpenCV-Backend implementiert. In
    Form der abstrakten \textit{ComputationBackend} Klasse wurde für diese beiden eine gemeinsame Schnittstelle geschaffen, die
    für jede Bearbeitungsmöglichkeit eines Bildes (Farbraumtransformation von RGB nach Grayscale, von RGB nach HSV,
    Anwendung des Emboss-Filters) eine akstrakte Methode enthält, die jeweils von den spezifischen Klassen
    \textit{OpenClComputationBackend} bzw. \textit{OpenCvComputationBackend} implementiert werden.

    \begin{figure}[ht]
		\includegraphics[width=0.93\linewidth]{./images/uml.pdf}
		\caption{Klassendiagramm der img-manipulator Applikation}
		\label{fig:uml}
	\end{figure}

    \pagebreak

    \subsection{CLI (Command-Line-Interface)}
    Das CLI dient zur Interaktion mit dem Programm und stellt die folgenden Optionen bereit:

    \begin{table}[ht]
    	\begin{tabularx}{\textwidth}{c|X}
    		\textbf{Argument} & \textbf{Beschreibung} \\ 
    		\hline
    		\texttt{-{}-}help o. \texttt{-}h 					 & \noindent\parbox[c]{\hsize}{Stellt alle verfügbaren 
                                                                   Optionen sowie jeweils eine kurze Beschreibung in 
                                                                   Form eines Hilfemenüs dar} \\  
    		\hline
    		\texttt{-{}-}task o. \texttt{-}t  					 & \noindent\parbox[c]{\hsize}{Definiert die von der 
                                                                   Anwendung ausgeführte Modifikation des Bildes. 
                                                                   Akzeptierte Werte sind rgb-to-grayscale, rgb-to-hsv
                                                                   (default) 
                                                                   und emboss-filter} \\  
    		\hline
    		\texttt{-{}-}backend o. \texttt{-}b 				 & \noindent\parbox[c]{\hsize}{Definiert das von der 
                                                                   Anwendung genutzte Berechnungsbackend. Akzeptierte 
                                                                   Werte sind opencl (default) und opencv} \\  
    		\hline
    		\texttt{-{}-}input-path o. \texttt{-}i 				 & \noindent\parbox[c]{\hsize}{Eingabe des Pfades des 
                                                                   zu bearbeitenden Bildes} \\  
    		\hline
    		\texttt{-{}-}output-path o. \texttt{-}o 			 & \noindent\parbox[c]{\hsize}{Eingabe des Pfades, an 
                                                                   dem das bearbeitete Bild abgelegt werden soll. Ist 
                                                                   dieser leer, wird die Berechnung durchgeführt aber 
                                                                   am Ende kein Bild gespeichert.} \\  
    		\hline
    		\texttt{-{}-}platform-id o. \texttt{-}p 			 & \noindent\parbox[c]{\hsize}{Angabe der von OpenCL 
                                                                   genutzten Plattform (siehe 
                                                                   \texttt{-{}-}print-opencl-platforms für mögliche 
                                                                   Werte)} \\  
    		\hline
    		\texttt{-{}-}device-id o. \texttt{-}d 				 & \noindent\parbox[c]{\hsize}{Angabe des von OpenCL
                                                                   genutzten Gerätes (siehe 
                                                                   \texttt{-{}-}print-opencl-devices für mögliche 
                                                                   Werte)} \\  
    		\hline
    		\texttt{-{}-}print-opencl-platforms 				 & \noindent\parbox[c]{\hsize}{Ausgabe aller verfügbaren
                                                                   OpenCL-Plattformen} \\  
    		\hline
    		\texttt{-{}-}print-opencl-devices 					 & \noindent\parbox[c]{\hsize}{Ausgabe aller
                                                                   verfügbaren OpenCL-Geräte für die spezifizierte
                                                                   Plattform} \\  
    		\hline
    		\texttt{-{}-}no-benchmark-repeat-calc o. \texttt{-}r & \noindent\parbox[c]{\hsize}{Anzahl an Wiederholungen
                                                                   der Berechnung für das Benchmarking}
    	\end{tabularx}
    	\caption{Verfügbare Parameter des Kommandozeilen-Interfaces}
    	\label{tab:cli_args}
    \end{table}

    Die Implementierung des CLIs erfolgte mithilfe des program\_options Moduls der Boost-Bibliothek. Diese übernimmt das
    Parsen der Eingabe mithilfe eines vom Programmierer konfigurierten Parsers. Im Falle des img-manipulators befindet
    sich die komplette Konfiguration des Parsers in der CliOptions-Klasse in der cli/cli\_options.hpp bzw.
    cli/cli\_options.cpp. Diese Klasse erhält als Konstruktorparameter die Kommandozeilenparameter und gibt diese an den
    konfigurierten Parser weiter. Die vom Parser ermittelten Werte werden dann in Datenfeldern der Klasse abgelegt,
    deren Werte mithilfe von Getter-Funktionen von außen zugänglich sind.

    \subsection{Berechnungs-Backends}
	Um den Wechsel zwischen den beiden Berechnungs-Backends zu ermöglichen, wurde mit der abstrakten 
    \textit{ComputationBackend}-Klasse eine gemeinsame Schnittstelle geschaffen. Diese enthält wie in
    \autoref{sec:aufbau}
	bereits beschrieben jeweils eine abstrakte Methode für die drei Manipulationsaufgaben und eine Methode, mit der 
	eventuelle noch offene Ressourcen wieder freigegeben werden können bevor das Objekt zerstört wird.

    \subsubsection{Mathematische Grundlagen} \label{sec:mathe_grundlagen}
	Im Folgenden werden zunächst die mathematischen Grundlagen der drei verschiedenen Manipulationsaufgaben erläutert 
	bevor anschließend deren Implementierung in den beiden Backendklassen unter Nutzung von OpenCL bzw. OpenCV näher 
	beschrieben wird.

    \paragraph{Farbraumtransformation RGB zu Grayscale}
	Der Farbraumtransformation von RGB zu Grayscale liegt folgende Formel zugrunde, mit der sich der Grauwert eines 
	Pixels auf Basis seiner Rot-, Grün- und Blauwerte berechnen lässt: $Grau = 0.21*R+0.72*G+0.07*B$. Da diese 
 	Berechnung für jeden Pixel und dessen Kanäle einmal durchgeführt wird, hat der Algorithmus eine Komplexität von 
	$O(n)$.

	Das Ergebnis einer solchen Farbraumtransformation ist in \autoref{fig:grayscale_transformation} dargestellt.

	\begin{figure}[ht]
		\centering
    	\begin{subfigure}[c]{0.35\textwidth}
    	    \includegraphics[width=\linewidth]{./images/dice.png}
    	    \subcaption{Original dice.png Bild}

    	\end{subfigure}
    	\begin{subfigure}[c]{0.35\linewidth}
    	    \includegraphics[width=\linewidth]{./images/grayscale.png}
    	    \subcaption{dice.png Bild nach der Grayscale Transformation}
    	\end{subfigure}
    	\caption{Beispiel für eine Grayscale Transformation}
		\label{fig:grayscale_transformation}
    \end{figure}

	\paragraph{Farbraumtransformation RGB zu HSV}
	Für die Farbraumtransformation von RGB zu HSV müssen basierend auf den drei Farbkanälen Rot, Grün und Blau die drei
	Werte Hue, Saturation und Value berechnet werden. Für diese Berechnung werden zunächst die Pixelwerte normalisiert 
	und anschließend das Minimum und das Maximum der Werte der Farbkanäle eines Pixels ermittelt. Diese beiden 
	Schritte lassen sich mathematisch wie folgt darstellen: 

	\begin{displaymath}
		R,G,B \in [0,1]
	\end{displaymath}
	\begin{displaymath}
		MAX := \textup{max}(R,G,B), MIN := \textup{min}(R,G,B)
	\end{displaymath}

	Anschließend wird Hue in Abhängigkeit davon berechnet, ob der ermittelte Maximalwert dem ermittelten Minimalwert 
	entspricht bzw. welchem Farbkanal der ermittelte Maximalwert angehört:

	\begin{displaymath}
        H := \left\{\begin{matrix}
        	0, 								 & \textup{falls}\; MAX = MIN \\ 
            60° * (0 +  \frac{G-B}{MAX-MIN}), & \textup{falls}\; MAX = R \\ 
            60° * (2 +  \frac{B-R}{MAX-MIN}), & \textup{falls}\; MAX = G \\ 
            60° * (4 +  \frac{R-G}{MAX-MIN}), & \textup{falls}\; MAX = B
        \end{matrix}\right.
	\end{displaymath}

	Der Sättigungswert (Saturation) des Pixels wird ebenfalls mit dem ermittelten Maximum und Minimum berechnet. So
	nimmt dieser den Wert 0 an, falls der Maximalwert 0 ist. Andernfalls wird die Differenz zwischen Maximum und 
	Minimum ins Verhältnis zum Maximum gesetzt. Als Formel lässt sich diese Berechnung so ausdrücken:
	
	\begin{displaymath}
        S := \left\{\begin{matrix}
        	0, 					  & \textup{falls}\; MAX = 0 \\ 
            \frac{MAX-MIN}{MAX}), & \textup{sonst}
        \end{matrix}\right.
	\end{displaymath}

	Der dritte Wert Value des Pixels entspricht dem ermittelten Maximum:
	\begin{displaymath}
        V := MAX
	\end{displaymath}

	Das Ergebnis einer solchen Farbraumtransformation zu HSV ist in \autoref{fig:hsv_transformation} zu sehen.
	
	\begin{figure}[ht]
		\centering
    	\begin{subfigure}[c]{0.35\textwidth}
    	    \includegraphics[width=\linewidth]{./images/dice.png}
    	    \subcaption{Original dice.png Bild}

    	\end{subfigure}
    	\begin{subfigure}[c]{0.35\linewidth}
    	    \includegraphics[width=\linewidth]{./images/hsv_cl.png}
    	    \subcaption{dice.png Bild nach der HSV Transformation}
    	\end{subfigure}
    	\caption{Beispiel für eine HSV Transformation}
		\label{fig:hsv_transformation}
    \end{figure}

	\paragraph{Emboss-Filter}
	Die Anwendung des Emboss-Filters kann durch mehrere unterschiedliche Berechnungen erfolgen. Im Folgenden wird die 
	im Rahmen dieser Belegarbeit genutzte Berechnung erläutert.

	Für diese wird neben dem aktuellen Pixel auch dessen Nachbarpixel, der sich links oben neben ihm befindet,
	benötigt. Die Werte der Farbkanäle dieser beiden Pixel werden jeweils voneinander subtrahiert. Diejenige der drei 
	berechneten Differenzen, die den größten absoluten Wert besitzt, wird anschließend für die Berechnung des 
	Grauwertes des aktuellen Pixels verwendet. So errechnet sich der Grauwert durch die Summe dieser Differenz und 128.
	Ist die resultierende Summe größer als 255, so erhält der Grauwert den Wert 255. Ist die Summe kleiner als 0, so 
	wird er 0 gesetzt. Der berechnete Wert wird anschließend allen drei Farbkanälen des Pixels zugewiesen. Der Wert des 
	Alphakanals des Inputs wird beibehalten.

	Das Ergebnis der Anwendung dieses Emboss-Filters ist in \autoref{fig:applied_emboss} abgebildet.

	\begin{figure}[ht]
		\centering
    	\begin{subfigure}[c]{0.35\textwidth}
    	    \includegraphics[width=\linewidth]{./images/dice.png}
    	    \subcaption{Original dice.png Bild}

    	\end{subfigure}
    	\begin{subfigure}[c]{0.35\linewidth}
    	    \includegraphics[width=\linewidth]{./images/emboss.png}
    	    \subcaption{dice.png Bild nach der Anwendung des Emboss-Filters}
    	\end{subfigure}
    	\caption{Beispiel für eine Anwendung des Emboss-Filters}
		\label{fig:applied_emboss}
    \end{figure}

    \subsubsection{OpenCL-Backend}
    Das OpenCL-Backend verwendet für alle Berechnungen ausschließlich auf der Grafikkarte ausgeführte Implementierungen
    der drei Manipulationsalgorithmen. Neben den drei Methoden für die Manipulationsalgorithmen verfügt die Klasse
    \textit{OpenClComputationBackend} auch
    über einige weitere Methoden. Diese ermöglichen unter anderem die Ausgabe verfügbarer Plattformen und Geräte sowie
    die Initialisierung der OpenCL-Pipeline.

    \paragraph{Pipeline-Initialisierung}
    Der Konstruktor der Klasse \textit{OpenClComputationBackend} ruft zur Initialisierung der OpenCL-Pipeline die
    \textit{setup\_opencl()}
    Methode auf. Diese bereitet alle Ressourcen für die Nutzung vor. Dies bedeutet im Detail, dass sie die
    OpenCL-Plattform und das OpenCL-Gerät auswählt und für diese einen Context erstellt (Zeilen 2-4 in
    \autoref{listing:setup_opencl}). Darüber hinaus lädt sie den Source Code, welcher auf der Grafikkarte ausgeführt werden soll
    (Zeilen 6-12) und
    kompiliert diesen (Zeilen 14-24). Abschließend wird die CommandQueue, welche zur Kommunikation mit der Grafikkarte
    benötigt wird, erstellt (Zeile 26).  

    \begin{listing}[ht]
        \begin{minted}[xleftmargin=20pt, linenos]{cpp}
void OpenClComputationBackend::setup_opencl() {
    platform = get_available_platforms()[platform_id];
    device = get_available_devices(platform)[device_id];
    context = cl::Context(device);

    auto exe_path = boost::filesystem::read_symlink("/proc/self/exe");
    auto kernel_file_path = exe_path.parent_path() / "kernels.cl";
    std::ifstream kernel_stream(kernel_file_path.string());
    std::string kernel_src(
            (std::istreambuf_iterator<char>(kernel_stream)),
            std::istreambuf_iterator<char>());
    sources.push_back({kernel_src});

    program = cl::Program(context, sources);
    try {
        program.build({device});
    }
    catch (const cl::BuildError &e) {
        for (auto const &error_pair: e.getBuildLog()) {
            std::cout << error_pair.second << std::endl;
        }
        throw std::runtime_error(
            "Compiling the kernel code caused an fatal error");
    }

    queue = cl::CommandQueue(context, device);
}
        \end{minted}
        \caption{Initialisierung der OpenCL Pipeline durch die \textit{setup\_opencl()} Methode}
        \label{listing:setup_opencl}
    \end{listing}

    \pagebreak

    \paragraph{Ausführung einer Kernelfunktion}

    Alle drei Manipulationsfunktionen (\textit{rgba\_to\_hsva()}, \textit{rgba\_to\_grayscale()} und
    \textit{apply\_emboss\_filter()}) rufen eine explizite Funktion aus dem Kernel-Sourcecode auf. Die Signaturen dieser
    drei Kernelmethoden ist bei allen drei Methoden gleich, weswegen für die Erstellung der Buffer und das Aufrufen der
    Kernelmethode eine allgemeine private Methode namens \textit{run\_kernel()} erstellt wurde.

    Diese Methode ist in \autoref{listing:run_kernel} zu sehen und erstellt wie bereits erwähnt zunächst jeweils einen Buffer für das
    Eingabe- und Ausgabebild auf der GPU (Zeilen 6-9).  Anschließend erzeugt sie ein Kernel-Objekt, indem sie den
    kompilierten Quelltext mit der gewünschten Einstiegskernelmethode kombiniert und dann deren benötigte Argumente
    setzt (Zeilen 11-13). 

    Im nächsten Schritt wird dann das Eingabebild in den Speicher der Grafikkarte kopiert (Zeilen 15-20). 

    Ist dies geschehen, wird das Kernel-Objekt dazu genutzt, die Berechnung zu starten (Zeilen 22-23). Die globale Worksize wird dabei
    durch Breite * Höhe des Bildes definiert, wobei die Breite des Bildes die erste und die Höhe die zweite der zwei
    Dimensionen darstellt. Dies bedeutet, dass die Parallelisierung der Berechnung für jeden Pixel erfolgt. Dabei ist
    allerdings zu beachten, dass die Anzahl an anforderbaren Work-Items durch die Anzahl an zur Adressierung genutzten
    Bits von der Grafikkarte limitiert ist
    \footnote{https://www.khronos.org/registry/OpenCL/sdk/1.1/docs/man/xhtml/clEnqueueNDRangeKernel.html}. Bei unseren
    zum Testen genutzten Plattformen konnten wir feststellen, dass alle uns zur Verfügung stehenden dedizierten
    Grafikkarten (Nvidia 980ti und Nvidia 920mx) in der Lage waren, 64 Bit für die Adressierung zu nutzen. So
    akzeptieren diese bis zu $2^{64}$ Work-Items. Die APUs unserer beiden Laptops dagegen, welche beide über eine Intel
    iGPU verfügen, nutzen nur 32 Bit zur Adressierung. Auf die manuelle Wahl einer spezifischen Lokalen Worksize haben
    wir verzichtet. Dies hat zur Folge, dass die OpenCL-Platform selbstständig eine Gruppierung vornimmt, welche einer
    gleichmäßigen Verteilung der Daten auf die Compute Units entspricht. Dies entspricht in unserem Fall der optimalen
    Verteilung, da kein geteilter Speicherzugriff zwischen verschiedenen Work-Items für unsere Algorithmen nötig ist und
    diese daher nicht in speziellen lokalen Gruppen zusammengefasst werden müssen.

    Nach der Bearbeitung und als letzter Schritt wird dann das bearbeitete Bild aus dem Speicher der GPU zurück in den 
    Speicher der CPU geschrieben (Zeilen 25-30).

    \begin{listing}[ht]
        \begin{minted}[xleftmargin=20pt, linenos]{cpp}
void OpenClComputationBackend::run_kernel(cv::Mat &input, cv::Mat &output,
            const std::string& kernel_name) {
    int width = input.size().width;
    int height = input.size().height;

    cl::Image2D input_cl_buffer(context, CL_MEM_READ_ONLY, 
            cl::ImageFormat(CL_RGBA, CL_UNORM_INT8), width, height);
    cl::Image2D output_cl_buffer(context, CL_MEM_READ_WRITE, 
            cl::ImageFormat(CL_RGBA, CL_UNORM_INT8), width, height);

    cl::Kernel kernel(program, kernel_name.c_str());
    kernel.setArg(0, input_cl_buffer);
    kernel.setArg(1, output_cl_buffer);

    queue.enqueueWriteImage(
            input_cl_buffer,
            CL_TRUE,
            {0, 0, 0},
            (unsigned long)width, (unsigned long)height, 1},
            0, 0, input.data);

    queue.enqueueNDRangeKernel(
            kernel, cl::NullRange, cl::NDRange(width, height));

    queue.enqueueReadImage(
            output_cl_buffer,
            CL_TRUE,
            {0, 0, 0},
            (unsigned long)width, (unsigned long)height, 1},
            0, 0, output.data);
}
        \end{minted}
        \caption{Allgemeine Funktion \textit{run\_kernel()} zur Ausführung von Kernelmethoden in \textit{OpenClComputationBackend}}
        \label{listing:run_kernel}
    \end{listing}


    \paragraph{Kernelfunktionen}

    Alle implementierten Kernelfunktionen befinden sich in der kernels.cl Datei. Ausschnitte aus dieser sind in
    \autoref{listing:kernels_cl} zu sehen. Ebenso befindet sich in dieser Datei neben den
    Kernelfunktionen und deren Hilfsmethoden auch die Definition des von allen Methoden verwendeten Samplers (Zeilen 1-2). Ein solcher Sampler definiert, wie Pixel aus dem \textit{image2d\_t} Datentyp gelesen und geschrieben werden. In
    unserem Fall haben wir uns dafür entschieden, alle Optionen so zu wählen, dass keinerlei Interpolation der Bilddaten
    während des Zugriffes auf diese erfolgt (\textit{CLK\_FILTER\_NEAREST}), der Zugriff über die normalen Pixelkoordinaten erfolgt
    (\textit{CLK\_NORMALIZED\_COORDS\_FALSE}) und bei dem Versuch, nicht verfügbare Pixel zu nutzen, die Kanten als Ausgabewert
    verwendet werden (\textit{CLK\_ADDRESS\_CLAMP\_TO\_EDGE}).  

    Die aufrufbaren Kernelfunktionen sind \texit{rgba\_to\_hsva()} (Zeilen 6-15), \textit{rgba\_to\_grayscale()} (Zeilen
    17-20) und
    \textit{rgba\_emboss\_filter()} (Zeilen 22-25). Diese Funktionen implementieren die in \autoref{sec:mathe_grundlagen} beschriebenen
    Algorithmen. Aus diesem Grund, wird an dieser Stelle auch nicht näher auf die eigentlichen Algorithmen eingegangen,
    sondern auf Besonderheiten, die der Compute Language geschuldet sind.

    Jeder Thread, bzw. jedes Work-Item erhält den von ihm zu bearbeitenden Pixel über dessen Global Ids (Zeilen 8-9).
    Diese können
    mithilfe der \textit{get\_global\_id()} Methode abgefragt werden. Als Parameter übergibt man zusätzlich noch die
    gewünschte Dimension (0 für x und 1 für y).

    Das Auslesen der Bilddaten erfolgt bei dem \textit{image2d\_t} Datentyp wie oben bereits erwähnt, mit Hilfe eines
    sogenannten Samplers (Zeile 11). Dieser Sampler wird als Parameter zusammen mit der gewünschten Position der
    \textit{read\_imagef()} Methode übergeben. Diese liefert dann die Daten an dieser Position als normalisierte
    Float-Werte im Bereich von 0-1. Neben der \textit{read\_imagef()} Methode gibt es noch einige andere Varianten,
    welche verschiedenste Rückgabewerte ermöglichen. So gibt z.B. \textit{read\_imagei()} die Werte als Integer-Werte
    zurück.

    Das Schreiben der Bilddaten ähnelt sehr stark dem Auslesen der Bilddaten. Wie bereits beim Lesen wird der Methode
    zum Schreiben ein Sampler und die Koordinaten übergeben. Zusätzlich werden allerdings noch die zu schreibenden Daten
    benötigt. Die Methode, welche das Schreiben übernimmt, heißt in unserem Fall \textit{write\_imagef()} (Zeile 14). Wie bereits 
    beim Auslesen gibt es auch von dieser Methode mehrere Varianten.
    
    \begin{listing}[ht]
        \begin{minted}[xleftmargin=20pt, linenos]{glsl}
__constant sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE 
            | CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_NEAREST;

... // other methods

__kernel void rgba_to_hsva(read_only image2d_t rgba_image,
            write_only image2d_t hsva_image) {
    const int x = get_global_id(0);
    const int y = get_global_id(1);

    const float4 rgba_values = read_imagef(rgba_image, sampler, (int2)(x, y));
    ... // calculation

    write_imagef(hsva_image, (int2)(x, y), hsva_values);
}

__kernel void rgba_to_grayscale(read_only image2d_t rgba_image,
            write_only image2d_t grayscale_image) {
    ...
}

__kernel void rgba_emboss_filter(read_only image2d_t rgba_image,
            write_only image2d_t grayscale_image) {
    ...
}

        \end{minted}
        \caption{Ausschnitte aus der kernels.cl Datei}
        \label{listing:kernels_cl}
    \end{listing}

    \pagebreak

    \subsubsection{OpenCV-Backend}
    Das OpenCV-Backend verwendet für alle Berechnungen OpenCV-Methoden, welche auf der CPU ausgeführt werden. Es dient
    primär als Möglichkeit des Vergleichs der Performance des OpenCL-Backends.

    Für die Farbraumtransformationen stellt OpenCV eine Methode bereit, die diese Aufgaben direkt erfüllt. So rufen die
    Methoden \texit{rgba\_to\_grayscale()} und \textit{rgba\_to\_hsva()} des \textit{OpenCvComputationBackends} OpenCVs
    \textit{cvtColor()} Methode mit dem Input, dem Output und dem jeweils gewünschten Farbraum als Parameter auf.

    Bei der Transformation in den HSV-Farbraum ist dabei zu beachten, dass diese Methode von OpenCV den Werteberech des
    roten Farbkanals auf Werte zwischen 0 und 180 beschränkt hat
    \footnote{https://docs.opencv.org/4.1.0/df/d9d/tutorial_py_colorspaces.html}, weshalb das Resultat einen Unterschied
    zu dem mithilfe des \textit{OpenClComputationBackends} erzielten aufweist. Dieser Unterschied ist in
    \autoref{fig:hsv_differences} zu sehen, welche eine Gegenüberstellung des gleichen Inputbildes enthält, das einmal
    durch das \textit{OpenClComputationBackend} und einmal durch das \textit{OpenCvComputationBackend} in den HSV-Farbraum konvertiert
    wurde.

	\begin{figure}[ht]
		\centering
    	\begin{subfigure}[c]{0.35\textwidth}
    	    \includegraphics[width=\linewidth]{./images/hsv_cl.png}
            \subcaption{Durch das \textit{OpenClComputationBackend}}

    	\end{subfigure}
    	\begin{subfigure}[c]{0.35\linewidth}
    	    \includegraphics[width=\linewidth]{./images/hsv_cv.png}
            \subcaption{Durch das \textit{OpenCvComputationBackend}}
    	\end{subfigure}
    	\caption{Farbraumtransformation zu HSV}
		\label{fig:hsv_differences}
    \end{figure}

    Für die Anwendung des Emboss-Filters dagegen bietet OpenCV keine fertige Methode. Dieser musste daher noch einmal
    unter Nutzung von OpenCV selbst implementiert werden. Dabei wurde besonders Wert auf eine performante
    Implementierung gelegt, um einen fairen Vergleich zu ermöglichen.

    Alle drei Methoden des \textit{OpenCvComputationBackends} rufen zum Schluss die ebenfalls selbst implementierte Funktion
    \textit{restore\_alpha\_channel()} auf, die die Werte des Alphakanals des Inputbildes dem Alphakanal des
    Outputbildes wieder zuweist und so den Alphakanal des Bildes bei der Bearbeitung erhält.

    \section{Vergleich}
    In abschließenden Tests wurden die beiden ComputationBackends und deren für die Berechnungen benötigte Zeiten
    miteinander verglichen. Für diese Tests wurden folgende Testbilder unterschiedlicher Größe verwendet
    (\autoref{fig:test_images}):

	\begin{figure}[ht]
		\centering
    	\begin{subfigure}[b]{0.3\textwidth}
    	    \includegraphics[width=\linewidth]{./images/dice.png}
            \subcaption{dice.png (800x600) und dice\_large.png (1754x1554)}
    	\end{subfigure}
    	\begin{subfigure}[b]{0.3\linewidth}
    	    \includegraphics[width=\linewidth]{./images/Periodic_table_large.jpg}
            \subcaption{Periodic\_table\_large.png (1754x1554)}
    	\end{subfigure}
    	\begin{subfigure}[b]{0.3\linewidth}
    	    \includegraphics[width=\linewidth]{./images/world.jpg}
            \subcaption{world\_75p.jpg (16200x8100)}
    	\end{subfigure}
        \caption{Für den Vergleich verwendete Bilder und deren Größe in Pixel}
		\label{fig:test_images}
    \end{figure}

    Das kleinste Bild war dabei 800 * 600 Pixel und das größte Bild 16200 * 8100 Pixel groß. Bei dem Versuch, ein noch
    größeres Bild (21600 * 10800 Pixel) zu nutzen, ist eine Exception aufgetreten, da mit diesem Bild die maximale Größe
    des \textit{image2d\_t} Datentyps des OpenCL-Devices überschritten war. 

    Die Tests wurden auf einer Nvidia 980Ti (22 Compute Units, Max \textit{image2d\_t} 16384x16384, 6 GB VRAM)
    Grafikkarte und einem Intel 2500K Prozessor (4 Kerne, kein Hyperthreading und einer Taktrate von 4.2 Ghz auf allen 
    Kernen) ausgeführt. Jedes Bild wurde dabei jeweils 1-, 100- und 1000-mal mithilfe der beiden Backends auf die drei
    verschiedenen Arten transformiert. Die benötigte Zeit der Berechnung wurde dabei mithilfe einer
    \textit{HighResolutionClock} gemessen, wobei jeder Versuchsaufbau fünfmal durchgeführt und anschließend die
    durchschnittlich benötigte Zeit ermittelt wurde. Auf Basis dieser ermittelten durchschnittlich benötigten Zeiten
    wurde anschließend für jeden Versuchsaufbau (Kombination aus Bild, Manipulationaufgabe und Anzahl an Durchläufen)
    die Zeitdifferenz zwischen dem \textit{OpenCl-} und dem \textit{OpenCvComputationBackend} ermittelt. Die Ergebnisse
    sind in \autoref{tab:results} zu sehen.

    Es fällt auf, dass das \textit{OpenClComputationBackend} bei den beiden kleinsten Bildern für die
    Farbraumtransformation zu Grayscale noch mehr Zeit benötigte als das \textit{OpenCvComputationBackend}, bei jedem
    anderen Versuchsaufbau jedoch schneller war. 

    So zeigt es bereits bei dem kleinsten Testbild bei den komplexeren Berechnungen der Farbraumtransformation zu HSV
    und der Anwendung des Emboss-Filters bei 1000 Iterationen einen Zeitvorsprung von 55,32\% bzw. 55,53\% gegenüber der
    Berechnung auf der CPU. Dieser Vorsprung wurde größer, je größer die Testbilder wurden. So betrug er bei dem größten
    getesteten Bild (bei 100 Iterationen, da bei diesem Bild aufgrund seiner Größe und dem damit verbundenen Zeitaufwand
    keine 1000 Iterationen durchgeführt wurden) bei der Farbraumtransformation zu HSV 145,44\% und bei der Anwendung des
    Emboss-Filters sogar 184,90\%.  

    Auch bei der Farbraumtransformation zu Grayscale war mit der wachsenden Größe der Testbilder eine Verbesserung der
    benötigten Zeit des \textit{OpenClComputationBackends} gegenüber der des \textit{OpenCvComputationBackends} zu
    beobachten. So war die parallelisierte Berechnung auf der GPU erstmals bei dem zweitgrößten getesteten Bild
    (Periodensystem der Elemente, 6000 * 3300 Pixel) auch bei dieser Manipulationsaufgabe um 4,53\% (bei 1000
    Iterationen) schneller als die Berechnung auf der CPU. Mit dem größten Testbild konnte dieser Vorsprung auf 16,22\%
    (bei 100 Iterationen) ausgeweitet werden.

    Betrachtet man die Durchläufe mit unterschiedlich vielen Iterationen eines Bildes jeweils im Vergleich, so zeigt
    sich, dass die Durchläufe, mit nur einer Iteration im Schnitt pro Bild etwas langsamer sind. Dies lässt sich
    höchstwahrscheinlich darauf zurückführen, dass hier im ersten Durchlauf interne Buffer und Objekte innerhalb der
    Bibliotheken generiert werden müssen.

    \newpage

	\begin{table}[H]
		\centering
    	\includegraphics[width=1.05\linewidth]{./images/ergebnisse.pdf}
        \caption{Ergebnisse aller Testdurchläufe für den Vergleich der parallelisierten Berechnung auf der GPU mit der
        Berechnung auf der CPU}
		\label{tab:results}
    \end{table}
    
    \section{Fazit}

    Mit OpenCL ist die Parallelisierung von Berechnungen herstellerunabhängig auf der Grafikkarte möglich. Allerdings
    haben die im vorigen Abschnitt beschriebenen von uns durchgeführten Tests gezeigt, dass sich die Nutzung von OpenCL
    erst bei größeren Datenmengen bzw. komplexeren Berechnungen lohnt. So war ein Wachstum des zeitlichen Vorsprungs
    gegenüber dem \textit{OpenCVComputationBackend} zu beobachten je größer die Testbilder und je komplexer die
    Berechnungen wurden. War das \textit{OpenCvComputationBackend} bei den kleinsten Testbildern bei der einfachen
    Farbraumtransformation von RGB zu Grayscale noch schneller als das \textit{OpenClComputationBackend}, so wurde es
    von diesem jedoch schnell bei der Anwedung der komplexeren Manipulationsaufgaben (Farbraumtransformation zu HSV und
    Anwendung des Emboss-Filters) und bei Nutzung der größeren Testbilder überholt. Weiterhin war zu beobachten, dass
    die eigentliche Berechnungszeit auf der Grafikkarte sehr kurz ist im Vergleich zum Overhead der Übertragung der
    Bilder von und zu der Grafikkarte. So war diese während unserer Tests die meiste Zeit idle, die CPU jedoch dauerhaft
    zu 100\% ausgelastet.

    Aus diesem Grund, müssten hier weitere Ansätze untersucht werden, um diesen gewaltigen Overhead zu verringern. Dafür
    muss die Engstelle während der Berechnung allerdings zuvor erst identifiziert werden. Aufgrund des begrenzten
    Zeitrahmens während des Semesters, konnten wir uns hiermit allerdings nicht weiter beschäftigen.

    \clearpage

    \listoffigures
 
    \listoftables

    \listoflistings

\end{document}
